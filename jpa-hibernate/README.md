
# Apunte 0 - JPA (Java Persistence API)

JPA es otra API de persistencia de Java pero trabaja a un nivel más alto de abstracción comparado con JDBC. Se basa en el concepto de ORM (Object-Relational Mapping) que mapea objetos en tu aplicación a tablas en una base de datos. JPA facilita el trabajo con bases de datos relacionales sin requerir que escribas consultas SQL explícitas para cada operación.

## Características principales

* **Entidades:** Las clases de tu modelo se anotan como entidades (@Entity), lo que permite a JPA saber qué clases deben ser mapeadas a tablas en la base de datos.
* **EntityManager:** El EntityManager es el punto de entrada para realizar operaciones CRUD en tus entidades.
* **JPQL:** JPA introduce un lenguaje de consulta llamado JPQL (Java Persistence Query Language) que se utiliza para realizar consultas de manera similar a SQL pero orientado a objetos.

## Historia y Evolución

* **EJB 2.x Entity Beans:** Antes de JPA, los Entity Beans en EJB 2.x eran la opción estándar para la persistencia en Java, pero eran complejos y difíciles de manejar.

* **JPA 1.0:** Introducido en 2006 como parte de la especificación de Java EE 5, con el objetivo de simplificar la persistencia en Java.

* **JPA 2.0:** Lanzado en 2009 como parte de Java EE 6, agregó características como el Criteria API y las anotaciones de mapeo adicionales.

* **JPA 2.1:** Lanzado en 2013 con Java EE 7, incluyó nuevas características como los Stored Procedures, Converters y más.

* **JPA 2.2:** Lanzado en 2017, se centró en correcciones menores y actualizaciones.

## Importancia de JPA en el Ecosistema Java

* **Independencia del Proveedor:** JPA permite a los desarrolladores cambiar fácilmente entre diferentes proveedores de persistencia como Hibernate, EclipseLink y OpenJPA.

* **Desacoplamiento:** Al separar los objetos de negocio de la lógica de persistencia, JPA facilita una arquitectura más limpia y mantenible.

* **Productividad:** Con anotaciones simples y un lenguaje de consulta intuitivo (JPQL), los desarrolladores pueden realizar tareas complejas de persistencia de manera más eficiente.

* **Estándar de la Industria:** JPA ha ganado aceptación y es ampliamente utilizado en aplicaciones empresariales, convirtiéndose en un estándar de facto para la persistencia en Java.

## Configuración del Entorno

1. **Nuevo Proyecto:** Crea un nuevo proyecto Java en tu IDE.

2. **Inclusión de Dependencias:** Añade las dependencias necesarias para JPA en tu archivo de configuración de build (como pom.xml si estás utilizando Maven).

    ``` xml
    <!-- Dependencia para Hibernate (un proveedor popular de JPA) -->
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>5.x.x</version>
    </dependency>
    ```

3. **Archivo persistence.xml:** Crea un archivo persistence.xml en el directorio `src/main/resources/META-INF`. Este archivo es esencial para configurar aspectos como el proveedor de JPA y las propiedades de la base de datos.

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <persistence version="2.0" xmlns="http://java.sun.com/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd">
        <persistence-unit name="nombreDeTuUnidadDePersistencia">
            <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
            <properties>
                <!-- Configuración de la base de datos -->
                <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/nombreDeLaBaseDeDatos"/>
                <property name="javax.persistence.jdbc.user" value="nombreDeUsuario"/>
                <property name="javax.persistence.jdbc.password" value="contraseña"/>
                <property name="javax.persistence.jdbc.driver" value="com.mysql.cj.jdbc.Driver"/>

                <!-- Propiedades adicionales de Hibernate -->
                <property name="hibernate.dialect" value="org.hibernate.dialect.MySQLDialect"/>
                <property name="hibernate.show_sql" value="true"/>
                <property name="hibernate.hbm2ddl.auto" value="update"/>
            </properties>
        </persistence-unit>
    </persistence>

    ```

**Configuración de `persistence.xml` para MySQL con Hibernate**

Este archivo XML contiene la configuración necesaria para conectar la aplicación Java con una base de datos MySQL usando Hibernate como proveedor JPA.

### Propiedades de Configuración

* **nombreDeTuUnidadDePersistencia**: Es el nombre que utilizarás para referenciar esta unidad de persistencia en tu código.
* **javax.persistence.jdbc.url**: URL de la base de datos.
* **javax.persistence.jdbc.user**: Nombre de usuario para acceder a la base de datos.
* **javax.persistence.jdbc.password**: Contraseña para acceder a la base de datos.
* **javax.persistence.jdbc.driver**: Clase del controlador JDBC.

#### Propiedades Adicionales de Hibernate

Las propiedades adicionales de Hibernate (como `hibernate.dialect`, `hibernate.show_sql`, `hibernate.hbm2ddl.auto`) son opcionales y pueden ajustarse según tus necesidades.

1. **Configuración de la Base de Datos:** Asegúrate de tener una base de datos instalada (como MySQL) y configura las credenciales y la URL en persistence.xml.

2. **Creación de Entidades:** Crea tu primera entidad Java y anótala adecuadamente con `@Entity`, `@Id`, etc.

3. **EntityManager:** Utiliza `EntityManager` para realizar operaciones CRUD básicas en tu entidad.

## Conceptos Básicos de JPA - Mapeo

Una vez que hayas configurado tu entorno de desarrollo para JPA, el siguiente paso es entender algunos de los conceptos básicos que son fundamentales para trabajar con esta API. Estos conceptos te ayudarán a comprender cómo JPA maneja la persistencia y cómo puedes aprovechar sus características para desarrollar aplicaciones robustas.

### Entidades y Atributos

#### Entidades

Una entidad es una clase Java anotada con @Entity que representa una tabla en una base de datos relacional. Cada instancia de una entidad corresponde a una fila en esa tabla.

```java
@Entity
public class Usuario {
    // código de la clase
}
```

#### Atributos

Los atributos de la clase entidad representan las columnas de la tabla. Estos pueden ser simples como números y cadenas, o complejos como otras entidades o colecciones de entidades.

```java
@Entity
public class Usuario {
    @Id
    private Long id;
    private String nombre;
    private String email;
    // ...
}
```

### Conceptos Básicos de JPA - EntityManager

EntityManager es la interfaz a través de la cual interactúas con el contexto de persistencia en JPA. Es responsable de las operaciones CRUD (Crear, Leer, Actualizar, Eliminar) y otras transacciones.

#### Obtener un EntityManager

Puedes obtener una instancia de EntityManager a través de EntityManagerFactory.

```java
EntityManagerFactory emf = Persistence.createEntityManagerFactory("nombreDeTuUnidadDePersistencia");
EntityManager em = emf.createEntityManager();
```

#### Operaciones CRUD Básicas

* **Crear:** em.persist(objeto)

* **Leer:** em.find(ClaseEntidad.class, id)

* **Actualizar:** em.merge(objeto)

* **Eliminar:** em.remove(objeto)

### Contexto de Persistencia

El contexto de persistencia es un conjunto de entidades gestionadas por un EntityManager en un momento dado. Cuando una entidad es gestionada, cualquier cambio en su estado se sincroniza automáticamente con la base de datos.

#### Estados de una Entidad

![picture 0](../images/21d51f76859fca03858130c188f72e9716f0a0221194a5ef79051598ec12e65f.png)  

* **New/Transient:** La entidad ha sido instanciada pero aún no está siendo gestionada por el EntityManager.

* **Managed/Persistent:** La entidad está siendo gestionada por el EntityManager y cualquier cambio se sincroniza con la base de datos.

* **Detached:** La entidad fue gestionada anteriormente pero ahora ya no está asociada con un EntityManager.

* **Removed:** La entidad está marcada para ser eliminada de la base de datos.

## Anotaciones en JPA

Las anotaciones en JPA juegan un papel crucial al simplificar la configuración de la persistencia y el mapeo objeto-relacional. A continuación se describen algunas de las anotaciones más importantes que debes conocer para trabajar con JPA.

### `@Entity`

Indica que una clase es una entidad y se debe mapear a una tabla en la base de datos.

```java
@Entity
public class Persona {
    // ...
}
```

### `@Id`

Identifica la propiedad que actúa como la clave primaria en la tabla de la base de datos.

```java
@Id
private Long id;
```

### `@GeneratedValue`

Se utiliza para especificar cómo se generan los valores de la clave primaria. Comúnmente se usa con @Id.

```java
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Long id;
```

### `@Column`

Define las propiedades de una columna en la tabla como el nombre de la columna, si es única, si es nullable, etc.

```java
@Column(name = "nombre_completo", nullable = false)
private String nombre;
```

### `@Table`

Se utiliza para especificar detalles sobre la tabla a la que se mapeará la entidad, como el nombre de la tabla, esquema, índices, etc.

```java
@Entity
@Table(name = "personas")
public class Persona {
    // ...
}
```

## Operaciones CRUD (Create, Read, Update, Delete)

Las operaciones CRUD son las operaciones básicas de manipulación de datos en cualquier sistema de gestión de bases de datos. En JPA, estas operaciones se pueden realizar utilizando el `EntityManager`. A continuación se describen estas operaciones en detalle:

### Create (Crear)

Para crear una nueva entidad y guardarla en la base de datos, utilizamos el método `persist`.

```java
EntityManager em = emf.createEntityManager();
em.getTransaction().begin();
Usuario nuevoUsuario = new Usuario("nombre", "email@example.com");
em.persist(nuevoUsuario);
em.getTransaction().commit();
```

### Read (Leer)

Para leer una entidad de la base de datos, utilizamos el método `find`.

```java
EntityManager em = emf.createEntityManager();
Usuario usuarioExistente = em.find(Usuario.class, id);
```

### Update (Actualizar)

Para actualizar una entidad ya existente, utilizamos el método `merge`.

```java
EntityManager em = emf.createEntityManager();
em.getTransaction().begin();
usuarioExistente.setEmail("nuevo-email@example.com");
em.merge(usuarioExistente);
em.getTransaction().commit();
```

### Delete (Eliminar)

Para eliminar una entidad de la base de datos, utilizamos el método `remove`.

```java
EntityManager em = emf.createEntityManager();
em.getTransaction().begin();
Usuario usuarioAEliminar = em.find(Usuario.class, id);
em.remove(usuarioAEliminar);
em.getTransaction().commit();
```

Estas son las operaciones CRUD básicas en JPA. Al comprender estos fundamentos, estarás bien equipado para manejar la persistencia de datos en tus aplicaciones Java.

## Relaciones entre Entidades

Las relaciones entre entidades son uno de los aspectos más poderosos de JPA. Permiten mapear las relaciones complejas que existen entre las tablas de una base de datos a objetos en el mundo Java de una manera muy intuitiva.

### Relación Uno a Uno (@OneToOne)

La anotación `@OneToOne` se utiliza para mapear una relación uno a uno entre dos entidades.

```java
@Entity
public class Persona {
    @OneToOne
    private Pasaporte pasaporte;
}
```

### Relación Uno a Muchos (@OneToMany) y Muchos a Uno (@ManyToOne)

La anotación `@OneToMany` se utiliza para mapear una relación uno a muchos, mientras que `@ManyToOne` se utiliza para mapear una relación muchos a uno.

```java
@Entity
public class Departamento {
    @OneToMany(mappedBy = "departamento")
    private List<Empleado> empleados;
}

@Entity
public class Empleado {
    @ManyToOne
    private Departamento departamento;
}
```

### Relación Muchos a Muchos (@ManyToMany)

La anotación `@ManyToMany` se utiliza para mapear una relación muchos a muchos entre dos entidades.

```java
@Entity
public class Estudiante {
    @ManyToMany
    private List<Curso> cursos;
}

@Entity
public class Curso {
    @ManyToMany(mappedBy = "cursos")
    private List<Estudiante> estudiantes;
}
```

Al entender cómo utilizar estas anotaciones para representar relaciones entre entidades, podrás modelar de manera eficiente tu base de datos relacional y tu código Java.

## Herencia y Polimorfismo en JPA

JPA proporciona varias estrategias para mapear la herencia en las clases de entidades, lo cual permite también implementar polimorfismo en las consultas. A continuación, se describen algunas de estas estrategias:

### Estrategia de Tabla por Jerarquía (`@Inheritance`)

Utilizando la anotación `@Inheritance`, puedes especificar que una tabla única será utilizada para todas las clases en una jerarquía de herencia. 

```java
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Vehiculo {
    @Id
    private Long id;
    private String marca;
}

@Entity
public class Coche extends Vehiculo {
    private String tipoCoche;
}
```

### Estrategia de Tabla por Subclase (`@JoinedColumn`)

En esta estrategia, cada clase en la jerarquía de herencia se mapea a su propia tabla, pero estas tablas están vinculadas mediante una clave foránea.

```java
@Entity
public class Animal {
    @Id
    private Long id;
    private String nombre;
}

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Perro extends Animal {
    private String raza;
}
```

### Estrategia de Tabla por Clase Concreta

En esta estrategia, cada clase concreta (no abstracta) en la jerarquía de herencia se mapea a su propia tabla sin ninguna conexión entre ellas.

```java
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Figura {
    @Id
    private Long id;
    private String color;
}

@Entity
public class Circulo extends Figura {
    private double radio;
}
```

### Polimorfismo en Consultas

JPA permite hacer consultas polimórficas que pueden devolver objetos de varias clases de una jerarquía de herencia.

```java
List<Vehiculo> vehiculos = em.createQuery("SELECT v FROM Vehiculo v", Vehiculo.class).getResultList();
```

Al dominar estas estrategias de mapeo de herencia y el uso del polimorfismo, podrás diseñar modelos de datos más versátiles y realizar consultas más flexibles.

## Consultas en JPA

Las consultas en JPA se pueden realizar de diversas maneras, incluyendo JPQL (Java Persistence Query Language), Criteria API y consultas nativas SQL. A continuación, se exploran cada una de estas opciones.

### JPQL (Java Persistence Query Language)

JPQL es un lenguaje de consultas similar a SQL pero orientado a objetos, lo cual lo hace más natural para los desarrolladores de Java.

#### Consulta Básica

```java
List<Usuario> usuarios = em.createQuery("SELECT u FROM Usuario u", Usuario.class).getResultList();
```

#### Consulta con Parámetros

```java
List<Usuario> usuarios = em.createQuery("SELECT u FROM Usuario u WHERE u.nombre = :nombre", Usuario.class)
                            .setParameter("nombre", "John")
                            .getResultList();
```

### Criteria API

Criteria API proporciona una forma programática de crear consultas, lo cual es útil cuando las consultas son dinámicas.

```java
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaQuery<Usuario> query = cb.createQuery(Usuario.class);
Root<Usuario> root = query.from(Usuario.class);
query.select(root).where(cb.equal(root.get("nombre"), "John"));
List<Usuario> usuarios = em.createQuery(query).getResultList();
```

### Consultas Nativas SQL

Para los casos en que se necesita un control más detallado que el ofrecido por JPQL o Criteria API, JPA también soporta consultas SQL nativas.

```java
List<Usuario> usuarios = em.createNativeQuery("SELECT * FROM usuarios WHERE nombre = 'John'", Usuario.class).getResultList();
```

### Resultados de la Consulta

Los resultados de las consultas en JPA pueden devolverse como una lista de entidades, un único resultado o incluso como un conjunto de resultados.

```java
Usuario usuario = em.createQuery("SELECT u FROM Usuario u WHERE u.id = :id", Usuario.class)
                     .setParameter("id", 1L)
                     .getSingleResult();
```

Con un conocimiento sólido de cómo realizar consultas en JPA, puedes recuperar y manipular datos en tus aplicaciones Java de forma efectiva y eficiente.

## Transacciones en JPA

Las transacciones son fundamentales para asegurar la integridad de los datos en aplicaciones que utilizan bases de datos. En JPA, el objeto `EntityManager` es el responsable de gestionar las transacciones. A continuación, se describen las operaciones básicas relacionadas con transacciones en JPA.

### Iniciar una Transacción

Para iniciar una transacción, utilizamos el método `begin` del objeto `EntityManager`.

```java
EntityManager em = emf.createEntityManager();
em.getTransaction().begin();
```

### Confirmar una Transacción (Commit)

Una vez que todas las operaciones han sido realizadas, la transacción se confirma utilizando el método `commit`.

```java
em.getTransaction().commit();
```

### Deshacer una Transacción (Rollback)

Si se encuentra un error o si se necesita revertir las operaciones realizadas, se puede utilizar el método `rollback`.

```java
em.getTransaction().rollback();
```

### Transacciones en Bloques try-catch

Es una buena práctica manejar las transacciones dentro de bloques try-catch para manejar excepciones de forma efectiva.

```java
try {
    em.getTransaction().begin();
    // Operaciones CRUD aquí
    em.getTransaction().commit();
} catch (Exception e) {
    em.getTransaction().rollback();
    e.printStackTrace();
} finally {
    em.close();
}
```

### Atributos de Transacción en Contenedores (Opcional)

Si estás utilizando JPA en un entorno gestionado como un servidor de aplicaciones Java EE, los atributos de transacción podrían ser gestionados por el contenedor.

```java
@Stateless
public class MiServicio {
    @PersistenceContext
    private EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void miMetodo() {
        // Operaciones CRUD aquí
    }
}
```

Al comprender cómo se manejan las transacciones en JPA, podrás garantizar que tus aplicaciones sean robustas y mantengan la integridad de los datos.


## Caché y Rendimiento en JPA

El rendimiento es un aspecto crucial al desarrollar aplicaciones que utilizan JPA para interactuar con bases de datos. Una de las formas de optimizar el rendimiento es a través del uso de cachés. A continuación, se describen varias estrategias y técnicas para mejorar el rendimiento y el uso de la caché en JPA.

### Primer Nivel de Caché (Caché de Entidad)

En JPA, el primer nivel de caché está asociado con un `EntityManager` y está habilitado de forma predeterminada.

```java
EntityManager em = emf.createEntityManager();
Usuario u1 = em.find(Usuario.class, 1L);
Usuario u2 = em.find(Usuario.class, 1L);  // No realiza otra consulta SQL
```

### Segundo Nivel de Caché (Caché de Aplicación)

Para almacenar entidades a nivel de aplicación, puedes configurar un segundo nivel de caché.

```java
@Cacheable(true)
@Entity
public class Producto {
    // campos y métodos
}
```

### Consulta de Caché

Puedes almacenar el resultado de consultas para mejorar el rendimiento.

```java
Query query = em.createQuery("SELECT u FROM Usuario u");
query.setHint("javax.persistence.cache.storeMode", "REFRESH");
List<Usuario> usuarios = query.getResultList();
```

### Optimización de Consultas

Utilizar consultas JPQL eficientes y la paginación pueden mejorar significativamente el rendimiento.

```java
TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombre LIKE :nombre", Usuario.class);
query.setParameter("nombre", "John%");
query.setMaxResults(10);
List<Usuario> usuarios = query.getResultList();
```

### Lazy Loading y Batch Fetching

El uso de Lazy Loading y Batch Fetching puede minimizar el número de consultas SQL necesarias para cargar entidades y sus relaciones.

```java
@OneToMany(fetch = FetchType.LAZY)
@BatchSize(size = 5)
private List<Pedido> pedidos;
```

Siguiendo estas estrategias y técnicas, podrás optimizar el rendimiento de tus aplicaciones JPA, lo que resultará en una experiencia de usuario más fluida y eficiente.


## Mejores Prácticas y Patrones de Diseño en JPA

El uso efectivo de JPA va más allá del conocimiento de sus características; también implica seguir buenas prácticas y patrones de diseño que facilitan la mantenibilidad y el rendimiento de tu aplicación. A continuación, se describen algunas mejores prácticas recomendadas:

### Lazy Loading vs. Eager Loading

Determinar cuándo cargar las relaciones entre entidades puede tener un impacto significativo en el rendimiento de la aplicación.

```java
@ManyToOne(fetch = FetchType.LAZY)
private Departamento departamento;
```

### Uso de DTO (Data Transfer Objects)

A veces es más eficiente utilizar DTOs para recuperar solo los campos necesarios en lugar de cargar entidades completas.

```java
List<Object[]> resultados = em.createQuery("SELECT u.nombre, u.email FROM Usuario u").getResultList();
```

### Patrón de Diseño DAO (Data Access Object)

Encapsular el código de acceso a datos en clases DAO facilita la reutilización del código y la separación de responsabilidades.

```java
public class UsuarioDAO {
    public Usuario findById(Long id) {
        // Código para buscar un usuario
    }
}
```

### Uso de Unidades de Trabajo

Mantener transacciones lo más cortas posible al agrupar lógicamente las operaciones en unidades de trabajo.

```java
try {
    em.getTransaction().begin();
    // Realizar varias operaciones relacionadas
    em.getTransaction().commit();
} catch (Exception e) {
    em.getTransaction().rollback();
}
```

### Estrategias de Caché

El uso efectivo de cachés a nivel de aplicación o de JPA puede mejorar drásticamente el rendimiento.

```java
@Cacheable(true)
@Entity
public class Producto { /* ... */ }
```

Siguiendo estas mejores prácticas y patrones de diseño, podrás construir aplicaciones más eficientes, mantenibles y escalables utilizando JPA.

## Ejemplo JPA

```java
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPASample {
  public static void main(String[] args) {
    // Crear EntityManagerFactory
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence-unit");
    
    // Crear EntityManager
    EntityManager em = emf.createEntityManager();
    
    // Iniciar transacción
    em.getTransaction().begin();
    
    // Operación CRUD
    MyEntity entity = new MyEntity();
    entity.setName("Sample");
    em.persist(entity);
    
    // Commit
    em.getTransaction().commit();
    
    // Cerrar recursos
    em.close();
    emf.close();
  }
}

```

## Enlaces relacionados

* [Introducción a JPA](http://www.jtech.ua.es/j2ee/restringido/jpa/sesion01-apuntes.html)

* [Java Persistence API (JPA)](https://www.ibm.com/docs/es/was-liberty/nd?topic=overview-java-persistence-api-jpa)

