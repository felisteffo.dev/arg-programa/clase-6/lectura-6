# Clase 6

Materiales para la Sexta clase, en esta clase cubrimos:

- Conceptos de Mapeo Objeto Relacional
- JPA
- Hibernate

## Lista de Materiales

- [Apunte 08 - JPA (Java Persistence API)](./jpa-hibernate/README.md)

## Lista de Ejemplos

- ver ejemplos de JPA en proyecto Ejemplos Clase

## Estado general de la semana

Versión para publicación 2023.

***

## Software requerido

- Java
- Maven
- IntelliJ idea
- MySql
- SqLite
- DBeaver
- Docker

## Clonar el presente repositorio

``` bash
cd existing_repo
git remote add origin https://gitlab.com/felisteffo.dev/arg-programa/clase-6/lecturas-6.git
git branch -M main
git push -uf origin main
```

## Autores

Felipe Steffolani - basado en un material original creado para la Cátedra de Backend de Aplicaciones

## License

Este trabajo está licenciado bajo una Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visita [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es](!https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).
